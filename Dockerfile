FROM continuumio/anaconda3:2021.05

RUN mkdir /config
COPY ./conda_environment.yml /conda/conda_environment.yml

RUN conda env create -f /conda/conda_environment.yml

RUN echo "conda activate fastai" >> ~/.bashrc

